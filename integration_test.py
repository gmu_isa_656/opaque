from headers import *
from kex import *
from opaque_server import *
from nacl.bindings.utils import sodium_memcmp
from opaque_f import *


def test():
    pw=b"The password is Password"
    # register user
    rec = pw2rec(pw)

    # initiate login


    #     uint8_t alpha[DECAF_X25519_PUBLIC_BYTES];
    #     uint8_t r[DECAF_X25519_PRIVATE_BYTES];W
    alpha, r = opaque_newUser(pw)

    # server responds
    rsec = Opaque_RegisterSec()
    rpub = Opaque_RegisterPub()

    rsec, rpub = opaque_initUser(alpha, rsec, rpub)

    # user commits its secrets
    rrec = Opaque_UserRecord()
    # Todo
    rpub, rrec = opaque_registerUser(pw, r, rpub, rrec)

    # server "saves"
    opaque_saveUser(rsec, rpub, rrec)

    opaque_usrSession(pw, sec, pub)
    # Todo
    resp, sk = opaque_srvSession(pub, rec)
    dump(sk, 32, "sk_s: ")
    # Todo
    pk = opaque_userSessionEnd(resp, sec, pw)
    dump(pk, 32, "sk_u: ")
    if not sodium_memcmp(sk, pk):
        raise Exception('sk does not equal to pk')

    # authenticate both parties:
    # to authenticate the server to the user, the server sends f_sk(1)
    # to the user, which calculates f_pk(1) and verifies it's the same
    # value as sent by the server.

    su = opaque_f(sk, 1)
    us = opaque_f(pk, 1)
    dump(su, 32, "f_sk(1): ")
    dump(us, 32, "f_pk(1): ")
    if not sodium_memcmp(su, us):
        raise Exception('sk does not equal to pk')

    # to authenticate the user to the server, the user sends f_pk(2)
    # to the server, which calculates f_sk(2) and verifies it's the same
    # value as sent by the user.
    us = opaque_f(pk, 2)
    su = opaque_f(sk, 2)
    dump(us, 32, "f_pk(2): ")
    dump(su, 32, "f_sk(2): ")
    if not sodium_memcmp(su, us):
        raise Exception('sk does not equal to pk')


def dump(p, dlen, msg):
    print(msg)
    for i in range(0, dlen):
        print("%02x"%p[i])


if __name__ == "__main__":
    main()
