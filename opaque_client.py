from headers import *
# variant where the secrets of U never touch S unencrypted


class OpaqueClient:
    def __init__(self, pw):
        self.pw = pw
        self.sec = Opaque_UserSession_Secret()
        self.pub = Opaque_UserSession()

    def newUser(self, pw = None):
        """
        Generating user asymmetric key pair
        :param pw: OPTIONAL, plaintext password
        :return: user private key and public key.
        """
        if pw is None:
            pw = self.pw

        # Picking random number x_u for user-side
        r = utils.random(DECAF_X25519_PRIVATE_BYTES)
        # Generating (H^0(pw))^r
        h0 = hash.blake2b(pw, 32)
        # Generating h0^r
        alpha = bindings.crypto_scalarmult(h0, r)

        return alpha, r

    def opaque_usrSession(self, pw=None, sec=None, pub=None):
        """
        Generating user asymmetric key pair.
        :param pw: OPTIONAL, plaintext password
        :param sec: OPTIONAL,
        :param pub: OPTIONAL,
        :return: user public key and private key.
        """
        if pw is None:
            pw = self.pw
        if sec is None:
            sec = self.sec
        if pub is None:
            pub = self.pub

        pub.alpha, sec.r = self.newUser()

        # Picking random number x_u for user-side
        sec.x_u = utils.random(DECAF_X25519_PRIVATE_BYTES)

        # X_u := g^x_u
        S_pri = public.PrivateKey(sec.x_u)
        pub.X_u = S_pri.public_key.encode()

        # Sending (H^0(pw))^r and X_u to Server.
        return sec, pub

    def opaque_registerUser(pw, r, pub, rec):
        # (a) Checks that β ∈ G ∗ . If not, raise exception.
        Beta = decaf_255_point_t()
        if(DECAF_SUCCESS!=decaf_255_point_decode(Beta, pub.beta, DECAF_FALSE)):
            raise RuntimeError('decoding error.')

        # (b) Computes rw := H(pw, β^1/r )
        R = decaf_255_scalar_t()
        decaf_255_scalar_decode_long(R, r, 32)
        # r = 1/r
        if decaf_255_scalar_invert(R, R) != DECAF_SUCCESS):
            raise RuntimeError('invert failed.')

        # H0 = β^(1/r)
        decaf_255_point_t H0
        decaf_255_point_scalarmul(H0, Beta, R)

        uint8_t h0[32], rw[32]
        decaf_255_point_encode(h0, H0)


        # rw = H(pw, β^(1/r))
        state_buf = bindings.crypto_generichash_blake2b_init()
        bindings.crypto_generichash_blake2b_update(state_buf, pw)
        bindings.crypto_generichash_blake2b_update(state_buf, h0)
        rw = bindings.crypto_generichash_blake2b_final(state_buf, bindings.crypto_generichash_BYTES)

        # Generating salt
        rec.salt = utils.random(len(rec.salt))

        # crypto_pwhash_argon2id_OPSLIMIT_INTERACTIVE = crypto_pwhash_OPSLIMIT_INTERACTIVE as well as MEMLIMIT
        rw = bindings.crypto_pwhash_alg(len(rw), rw, rec.salt,
                                      bindings.crypto_pwhash_argon2i_OPSLIMIT_INTERACTIVE,
                                      bindings.crypto_pwhash_argon2i_MEMLIMIT_INTERACTIVE,
                                      bindings.crypto_pwhash_ALG_DEFAULT)
        if rw is None:
            raise Exception('out of memory.')

        # Generating user private key
        rec.c.U_pri = utils.random(DECAF_X25519_PRIVATE_BYTES)# random user secret key
        # Retrieving user public key
        rec.c.U_pub = public.PrivateKey(rec.c.U_pri).public_key.encode()

        # Copying user public key to plaintext rec
        rec.U_pub = rec.c.U_pub # size DECAF_X25519_PUBLIC_BYTES

        # Copying server public key to plaintext rec
        rec.c.S_pub = pub.S_pub # size DECAF_X25519_PUBLIC_BYTES

        # c ← AuthEnc_rw(U_pri,U_pub,S_pub)
        rec.c.nonce = utils.random(bindings.crypto_secretbox_NONCEBYTES)                      # nonce for crypto_secretbox

        rec.c.U_pri = bindings.crypto_secretbox(rec.c.U_pri, rec.c.nonce, rw)  # plaintext, nonce,key

        return 0, rec

    # Paper: taking (StorePwdFile, sid , U, pw)
    # (StorePwdFile, sid , U, pw): S computes k_s ←_R Z_q , rw := F_k_s (pw),
    # S_pri ←_R Z_q , U_pri ←_R Z_q , S_pub := g^S_pri , U_pub := g^U_pri , c ← AuthEnc_rw (U_pri, U_pub, S_pub)
    # it records file[sid ] := {k_s, S_pri, S_pub, U_pub, c}.
    def opaque_storePwdFile(pw):
        rec = Opaque_UserRecord()
        # k_s ←_R Z_q
        rec.k_s = utils.random(32)

        # rw := F_k_s (pw),
        rw = bytes(32)  # uint8_t
        rw = oprf(pw, rec.k_s)

        rec.salt = utils.random(len(rec.salt))
        # Todo rec.salt could be None. Then its len may <> the datatype it belongs to

        # the binding has checked the crypto_pwhash return. if the return is not 0, it will raise exception.
        rw = bindings.crypto_pwhash_alg(len(rw), rw, rec.salt,
                                      bindings.crypto_pwhash_argon2i_OPSLIMIT_INTERACTIVE,
                                      bindings.crypto_pwhash_argon2i_MEMLIMIT_INTERACTIVE,
                                      bindings.crypto_pwhash_ALG_DEFAULT)


        # public.PrivateKey.generate() is not better.
        # S_pri ←_R Z_q
        rec.S_pri = utils.random(DECAF_X25519_PRIVATE_BYTES)  # random server secret key

        # U_pri ←_R Z_q
        rec.c.U_pri = utils.random(DECAF_X25519_PRIVATE_BYTES)  # random user secret key

        # S_pub := g^S_pri
        rec.S_pub = public.PrivateKey(rec.S_pri).public_key

        # U_pub := g^U_pri
        rec.U_pub = public.PrivateKey(rec.c.U_pri).public_key

        #print(rec.U_pub, rec.U_pub.encode() )   use encode to get byte-ify value of publickey obj.

        # copy User pub key in server to User record rec.c in client
        rec.c.U_pub = rec.U_pub

        # c ← AuthEnc_rw(U_pri,U_pub,S_pub): Encrypt clients' <Pri_U, Pub_U, Pub_S> i.e. <Pri_U, g^Pri_U, g^Pri_S>
        rec.c.nonce = utils.random(bindings.crypto_secretbox_NONCEBYTES)  # nonce for crypto_secretbox
        #print(type(rec.c.U_pri), type(rec.c.U_pub), type(rec.c.S_pub))

        rec.c.U_pri = bindings.crypto_secretbox(rec.c.U_pri+rec.c.U_pub.encode()+rec.c.S_pub, rec.c.nonce, rw)

        # crypto_secretbox_easy binding is found at https://github.com/zachreizner/sodium
        '''
        crypto_secretbox_easy(((uint8_t*)&rec->c)+crypto_secretbox_NONCEBYTES,         # ciphertext
                              ((uint8_t*)&rec->c)+crypto_secretbox_NONCEBYTES,         # plaintext
                            DECAF_X25519_PRIVATE_BYTES+DECAF_X25519_PUBLIC_BYTES*2,  # plaintext len
                            ((uint8_t*)&rec->c),                                     # nonce
                            rw)                                                     # key
        '''

        return rec

    # 3. On β, X_s and c from S, U proceeds as follows:
    # (a) Checks that β ∈ G ∗ . If not, outputs (abort, sid , ssid ) and halts
    # (b) Computes rw := H(pw, β^1/r )
    # (c) Computes AuthDec_rw(c). If the result is ⊥, outputs (abort, sid , ssid ) and halts.
    #     Otherwise sets (U_pri, U_pub, S_pub ) := AuthDec_rw (c)
    # (d) Computes K := KE(U_pri, x_u, S_pub, X_s) and SK := f_K(0)
    # (e) Outputs (sid, ssid, SK).
    def opaque_userSessionEnd(resp, sec, pw):
        # (a) Checks that β ∈ G ∗ . If not, outputs (abort, sid , ssid ) and halts
        decaf_255_point_t
        Beta
        if (DECAF_SUCCESS != decaf_255_point_decode(Beta, resp->beta, DECAF_FALSE)) return 1

        # (b) Computes rw := H(pw, β^1/r )
        decaf_255_scalar_t
        r
        decaf_255_scalar_decode_long(r, sec->r, 32)
        # r = 1/r
        if (decaf_255_scalar_invert(r, r) != DECAF_SUCCESS) return 1

        # H0 = β^(1/r)
        decaf_255_point_t
        H0
        decaf_255_point_scalarmul(H0, Beta, r)
        decaf_255_scalar_destroy(r)
        decaf_255_point_destroy(Beta)
        h0 = bytes(32)  # uint8_t
        rw = bytes(32)  # uint8_t
        decaf_255_point_encode(h0, H0)
        decaf_255_point_destroy(H0)

        # rw = H(pw, β^(1/r))
        state_buf = b''
        digest_size = bindings.crypto_generichash_BYTES
        state_buf = bindings.crypto_generichash_blake2b_init(state_buf, b'', b'', digest_size)
        state_buf = bindings.crypto_generichash_blake2b_update(state_buf, pw)
        state_buf = bindings.crypto_generichash_blake2b_update(state_buf, h0)
        rw = bindings.crypto_generichash_blake2b_final(state_buf, digest_size)

        decaf_bzero(h0, sizeof(h0))
        decaf_bzero( & state, sizeof(state))


        rw = bindings.crypto_pwhash_alg(len(rw), rw, resp.salt,
                                        bindings.crypto_pwhash_argon2i_OPSLIMIT_INTERACTIVE,
                                        bindings.crypto_pwhash_argon2i_MEMLIMIT_INTERACTIVE,
                                        bindings.crypto_pwhash_ALG_DEFAULT)
        if rw is None:
            # out of memory
            return 1

        # (c) Computes AuthDec_rw(c). If the result is ⊥, outputs (abort, sid , ssid ) and halts.
        #     Otherwise sets (U_pri, U_pub, S_pub ) := AuthDec_rw (c)
        c = C()
        if (0 != crypto_secretbox_open_easy(((uint8_t *) & c) + crypto_secretbox_NONCEBYTES,  # plaintext
                                            ((uint8_t *) & resp->c) + crypto_secretbox_NONCEBYTES,  # ciphertext
                                            # plaintext len
                                            DECAF_X25519_PRIVATE_BYTES + DECAF_X25519_PUBLIC_BYTES * 2 + crypto_secretbox_MACBYTES,
                                            ((uint8_t *) & resp->c),  # nonce
                                            rw))  # key
            decaf_bzero(rw, sizeof(h0))
            return 1

        decaf_bzero(rw, sizeof(h0))

        # (d) Computes K := KE(U_pri, x_u, S_pub, X_s) and SK := f_K(0)
        sk = ser_kex(c.U_pri, sec.x_u, c.S_pub, resp.X_s)
        if u != 0:
            decaf_bzero( & c, sizeof(c)):
            return 1

        # (e) Outputs (sid, ssid, SK)

        return sk


    # Todo:
    #   check params' type at first.

    KEY_LENGTH = 32

    def oprf(x, k):
        """
        OPRF
        Implement the F_k(x) = H(x, (H0(x))^k)
        :param x: BYTES password from the user
        :param k: BYTES private_key from the server
        :return: E(H(x, (H(x))^k), g^k)
        """
        # F_k(x) = H(x, (H0(x)) ^ k) for key k ∈ Z_q

        # h0 = H'(x)
        #   uint8_t h0[32];
        #   crypto_generichash(h0, sizeof h0, x, x_len, 0, 0);
        #   decaf_255_point_t H0;
        #   decaf_255_point_from_hash_nonuniform(H0, h0);
        if isinstance(x, bytes):
            # in bytes, digest_len, out bytes.
            h0 = hash.blake2b(x, KEY_LENGTH)
        else:
            raise ValueError('oprf:param:x must be BYTE')

        # k -> Z_q
        #   decaf_255_scalar_t K;
        #   decaf_255_scalar_decode_long(K, k, DECAF_255_SCALAR_BYTES);
        # H0 ^ k
        #   decaf_255_point_t H0_k;
        #   decaf_255_point_scalarmul(H0_k, H0, K);
        #   decaf_255_scalar_destroy(K);
        #   decaf_255_point_destroy(H0);
        # h0: = (H0(x)) ^ k
        #   decaf_255_point_encode(h0, H0_k);
        #   decaf_255_point_destroy(H0_k);

        # (H0(x))^k  Corresponding public key of param k
        #   bindings.crypto_scalarmult(h0, k)
        #
        #   I have no idea why does the author use scalarmult but scalarmult_base,
        #   since the general procedure should be like the code in the test/schalar_test.py

        # crypto_scalarmult_base == crypto_scalarmult_curve25519_base
        #   in: bytes out: bytes
        #   raw_pub_k = bindings.crypto_scalarmult_base(k)
        h0_k = bindings.crypto_scalarmult(h0, k)

        # res = H(x, (H0(x))^k)
        state_buf = bindings.crypto_generichash.generichash_blake2b_init()
        # in: byte
        bindings.crypto_generichash.generichash_blake2b_update(state_buf, x)
        bindings.crypto_generichash.generichash_blake2b_update(state_buf, h0_k)
        res = bindings.crypto_generichash.generichash_blake2b_final(state_buf, bindings.crypto_generichash_BYTES)
        return res


if __name__ == "__main__":
    # testing newUser
    import os

    alpha, r = newUser(b'sjfpojwepog')
    print(alpha) if alpha is not False else print('error')

    # testing userSession
    sec = Opaque_UserSession_Secret()
    pub = Opaque_UserSession()

    resec, repub = opaque_usrSession(b'sgfwe16g58w14e', sec, pub)

    print(resec.__dict__.keys(), repub.__dict__.keys())
    print(repub.X_u, resec.r, resec.x_u) if repub else print('error')

    # testing registerUser

    import os

    sec = Opaque_RegisterSec()
    pub = Opaque_RegisterPub()

    alpha = opaque_newUser(b'sjfpojwepog')
    resec, repub = initUser.opaque_initUser(alpha, sec, pub)
    opaque_registerUser()

    #register

    # testing pw2rec
    import os

    rec = Opaque_UserRecord()
    rec = opaque_storePwdFile(b'sjfpojwepog', rec)
    print(rec) if rec is not False else print('error')

    print('c = AuthEnc_rw(U_pri,U_pub,S_pub): ', rec.c.U_pri)

    # testing oprf
    import os

    a = b'\xc0N\xa7\xd5\xa7\x82j\x87Dk\x92kR\x9b\xbd\xf0\x16\x9f\x86\x9f\xb1\x17\xa6\xe1\x0c\x1b\xa1\xd1c\x83r\x0f'
    # a = os.urandom(32)
    print(a)
    print(oprf(b'sjfpojwepog', a))