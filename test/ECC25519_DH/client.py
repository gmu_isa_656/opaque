from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PrivateKey, X25519PublicKey
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from binascii import *
import socket
from cryptography.hazmat.primitives import serialization

MAC = hashes.SHA256()
KEY_LENGTH = 32
SALT = None
ACTIVE = True

def passive_ECDHE(s, S_pub_k):
    C_prv_k = X25519PrivateKey.generate()
    C_pub_k = C_prv_k.public_key()

    skt_comm(s, C_pub_k)
    shared_key = C_prv_k.exchange(S_pub_k)
    # Perform key derivation.
    derived_key = HKDF(
        algorithm=MAC,
        length=KEY_LENGTH,
        salt=SALT,
        info=b'handshake data',
        backend=default_backend()
        ).derive(shared_key)
    print("SK: ", derived_key)


def active_ECDHE(s):
    # C_prv_k  = r
    C_prv_k = X25519PrivateKey.generate()
    # C_pub_k = g^r?
    C_pub_k = C_prv_k.public_key()

    C_pub_k_bytes = C_pub_k.public_bytes(
        encoding=serialization.Encoding.Raw,
        format=serialization.PublicFormat.Raw,
    )
    re = skt_comm(s, b'KE:' + C_pub_k_bytes)
    if re.startswith(b'KE:'):
        re = re[3:]
        S_pub_k = X25519PublicKey.from_public_bytes(re)
        #print(type(S_pub_k), S_pub_k)

        shared_k = C_prv_k.exchange(S_pub_k)


        derived_key = HKDF(
            algorithm=MAC,
            length=KEY_LENGTH,
            salt=SALT,
            info=b'handshake data',
            backend=default_backend()
            ).derive(shared_k)

        print("SK: ", derived_key)

'''
# For the next handshake we MUST generate another private key.
private_key_2 = X25519PrivateKey.generate()
peer_public_key_2 = X25519PrivateKey.generate().public_key()
shared_key_2 = private_key_2.exchange(peer_public_key_2)
derived_key_2 = HKDF(
    algorithm=hashes.SHA256(),
    length=32,
    salt=None,
    info=b'handshake data',
    backend=default_backend()
    ).derive(shared_key_2)
print(derived_key_2)
'''


def skt_comm(s, msg):
    while True:
        #msg = bytes(msg.strip(), encoding='utf8')
        print('Starting sending:\t{0}.'.format(msg))

        d = None
        try:
            s.send(msg)
        except Exception as e:
            print(e)
        print('Client -> Server:\t{0} bytes sent.'.format(str(len(msg))))
        while True:
            d = s.recv(1024)
            if d:
                print(b'Client <- Server:\t%b' % (d))
                return d



if __name__ == '__main__':
    if True:
        #input('Send:')
        h = socket.gethostname()
        p = 23330
        print('Connecting to {0}:{1}'.format(h, p))
        with socket.create_connection((h, p)) as s:
            print('Connection established .\n')
            if ACTIVE:
                active_ECDHE(s)
                s.send(b'q')
            else:
                while True:
                    d = s.recv(1024)
                    if d.startswith(b'PK:'):
                        d = d[3:]
                        print('Client <- Server:\t{0}'.format(str(d, encoding='ascii')))
                        passive_ECDHE(s, d)
