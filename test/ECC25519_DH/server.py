from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PrivateKey,X25519PublicKey
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from binascii import *
import socket
from cryptography.hazmat.primitives import serialization

MAC = hashes.SHA256() #or BLAKE2b
KEY_LENGTH = 32
SALT = None
ACTIVE = False

def passive_ECDHE(s, C_pub_k):
    S_prv_k = X25519PrivateKey.generate()
    S_pub_k = S_prv_k.public_key()

    S_pub_k_bytes = S_pub_k.public_bytes(
        encoding=serialization.Encoding.Raw,
        format=serialization.PublicFormat.Raw,
    )
    skt_comm(s, b'KE:' + S_pub_k_bytes, False)
    C_pub_k = X25519PublicKey.from_public_bytes(C_pub_k)

    shared_key = S_prv_k.exchange(C_pub_k)
    # Perform key derivation.
    derived_key = HKDF(
        algorithm=MAC,
        length=KEY_LENGTH,
        salt=SALT,
        info=b'handshake data',
        backend=default_backend()
        ).derive(shared_key)
    print("SK: ", derived_key)


def active_ECDHE(s):
    S_prv_k = X25519PrivateKey.generate()
    S_pub_k = S_prv_k.public_key()

    C_pub_k = skt_comm(s, S_pub_k)
    print(type(C_pub_k), C_pub_k)

    shared_k = S_prv_k.exchange(C_pub_k)

    derived_key = HKDF(
        algorithm=MAC,
        length=KEY_LENGTH,
        salt=SALT,
        info=b'handshake data',
        backend=default_backend()
    ).derive(shared_k)

    print("SK: ", derived_key)


def skt_comm(s, msg, need_resp=True):
    while True:
        #msg = bytes(msg.strip(), encoding='utf8')
        print('Starting sending:\t{0}.'.format(msg))

        d = None
        try:
            s.send(msg)
        except Exception as e:
            print(e)
        #print('Client <- Server:\t{0} bytes sent.'.format(str(len(msg))))

        while True and need_resp:
            d = s.recv(1024)
            if d:
                #print('Client -> Server:\t{0}'.format(str(d, encoding='ascii')))
                return d
        if not need_resp:
            break



def start_server(srvr_host, srvr_port):

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as s:
        print("Starting binding...")
        s.bind((srvr_host, srvr_port))
        s.listen(5)
        print("Binding done. Starting listening on {0}:{1}\n".format(srvr_host, srvr_port))
        if True:
            while True:
                conn, addr = s.accept()
                print('> {0[0]}:{0[1]}\t\tConnection accepted.'.format(addr))
                if ACTIVE:
                    active_ECDHE(conn)


                while True:
                    d = conn.recv(1024)
                    print('> {0[0]}:{0[1]}\t\t{1} bytes received'.format(addr, len(d)))
                    #conn.send(b'%b bytes received: %b' % (bytes(str(len(d)), encoding='ascii'), d))
                    if d == b'q':
                        conn.send(b'%b:%b, BYE.'% (bytes(str(addr[0]), encoding='ascii'), bytes(str(addr[1]), encoding='ascii')))
                        print('> {0[0]}:{0[1]}\t\tThe client terminates the connection.\n'.format(addr))
                        conn.close()
                        break
                    elif d.startswith(b'KE:'):
                        C_pub_k = d[3:]
                        passive_ECDHE(conn, C_pub_k)
                        conn.close()
                        break






if __name__ == '__main__':
    S_prv_k = X25519PrivateKey.generate().public_key()


    start_server(socket.gethostname(), 23330)



