from headers import *

# server_kex and user_kex are a pair of
# key exchange method used by the author of libspinx
# for retrieving serect key.
# Todo: try to use ECDH.

# server side triple-dh
def server_kex(mk, ix, ex, Ip, Ep):
    uint8_t sec[DECAF_X25519_PUBLIC_BYTES * 3], *ptr = sec

    if(DECAF_SUCCESS!=decaf_x25519(ptr,Ep,ix)):
        return 1
    ptr+=DECAF_X25519_PUBLIC_BYTES
    if(DECAF_SUCCESS!=decaf_x25519(ptr,Ip,ex)):
        return 1
    ptr+=DECAF_X25519_PUBLIC_BYTES
    if(DECAF_SUCCESS!=decaf_x25519(ptr,Ep,ex)):
        return 1

    derive_secret(mk, sec)

    return 0

# user side triple-dh
def user_kex(mk, ix, ex, Ip, Ep):
    sec = bytes(DECAF_X25519_PUBLIC_BYTES * 3)
    ptr = sec
    if(DECAF_SUCCESS!=decaf_x25519(ptr,Ip,ex)) return 1
    ptr+=DECAF_X25519_PUBLIC_BYTES
    if(DECAF_SUCCESS!=decaf_x25519(ptr,Ep,ix)) return 1
    ptr+=DECAF_X25519_PUBLIC_BYTES
    if(DECAF_SUCCESS!=decaf_x25519(ptr,Ep,ex)) return 1

    # and hash for the result SK = f_K(0)
    derive_secret(mk, sec)

    return 0

def derive_secret(sec):
    # workaround hash sec from 96 bytes down to 64,
    # as blake can only handle 64 as a key
    hashkey = bytes(64)  # uint8_t

    #crypto_generichash == crypto_generichash_blake2b. hash.blake2b == hash.generichash
    hashkey = hash.blake2b(sec, len(hashkey))

    # and hash for the result SK = f_K(0)
    opaque_f(hashkey, len(hashkey), 0, mk)

    return mk


