from nacl import hashlib, bindings, pwhash, utils, hash, public
import binascii
import random

from tools import type2type

KEY_LENGTH = 32

# [BlockA] The following definitions and structures are from:
# https://github.com/krionbsd/libdecaf/blob/24915bd8cc6332c3b6d038b1348db14794e2113c/src/GENERATED/include/decaf/point_255.h

# Number of bytes in a serialized point.
DECAF_255_SER_BYTES = 32

# Number of bytes in an elligated point.  For now set the same as SER_BYTES
# but could be different for other curves.
DECAF_255_HASH_BYTES = 32

# Number of bytes in a serialized scalar.
DECAF_255_SCALAR_BYTES = 32

# Number of bits in the "which" field of an elligator inverse
DECAF_255_INVERT_ELLIGATOR_WHICH_BITS = 5

# The cofactor the curve would have, if we hadn't removed it
DECAF_255_REMOVED_COFACTOR = 8

# X25519 encoding ratio.
DECAF_X25519_ENCODE_RATIO = 4

# Number of bytes in an x25519 public key
DECAF_X25519_PUBLIC_BYTES = 32

# Number of bytes in an x25519 private key
DECAF_X25519_PRIVATE_BYTES = 32

# The number of bits in a word of a 64bit computer.
DECAF_WORD_BITS = 64

DECAF_255_SCALAR_LIMBS = ((253-1)/DECAF_WORD_BITS+1)

class decaf_255_scalar_t:
    limb = list()
    #limb = list(DECAF_255_SCALAR_LIMBS)  # uint64_t (aka. unsigned long int) array

# [BlockA] End


# [BlockB] The following class are from opaque.h:

# User/Client
class C:
    class ConstError(TypeError):
        pass
    __lock = False
    nonce = bytes(bindings.crypto_secretbox_NONCEBYTES)  # uint8_t
    U_pri = bytes(DECAF_X25519_PRIVATE_BYTES)  # uint8_t
    U_pub = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    S_pub = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    mac = bytes(bindings.crypto_secretbox_MACBYTES)


# Server
# user specific record stored at server upon registration
class Opaque_UserRecord:
    k_s = bytes(DECAF_255_SCALAR_BYTES)  # uint8_t
    S_pri = bytes(DECAF_X25519_PRIVATE_BYTES)  # uint8_t
    S_pub = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    U_pub = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    salt = bytes(bindings.crypto_pwhash_SALTBYTES)
    # uint8_t   Libsodium restrict the salt length to 16bytes, while libsphinx is 32
    c = C()


# data sent to S from U in login#1
class Opaque_UserSession:
    alpha = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    X_u = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t


class Opaque_UserSession_Secret:
    r = bytes(DECAF_X25519_PRIVATE_BYTES)  # uint8_t
    x_u = bytes(DECAF_X25519_PRIVATE_BYTES)  # uint8_t



class Opaque_ServerSession:
    beta = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    X_s = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    salt = bytes(32)  # uint8_t
    c = C()


class Opaque_RegisterPub:
    beta = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t
    S_pub = bytes(DECAF_X25519_PUBLIC_BYTES)  # uint8_t



class Opaque_RegisterSec:
    S_pri = bytes(DECAF_X25519_PRIVATE_BYTES)  # uint8_t
    k_s = bytes(DECAF_X25519_PRIVATE_BYTES)  # uint8_t



















