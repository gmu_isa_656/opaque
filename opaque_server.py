from headers import *

# 2. (SvrSession, sid , ssid ): On input α from U, S proceeds as follows:
# (a) Checks that α ∈ G^∗ If not, outputs (abort, sid , ssid ) and halts
# (b) Retrieves file[sid] = {k_s, S_pri, S_pub, U_pub, c}
# (c) Picks x_s ←_R Z_q and computes β := α^k_s and X_s := g^x_s
# (d) Computes K := KE(S_pri, x_s, U_pub, X_u) and SK := f K (0)
# (e) Sends β, X s and c to U
# (f) Outputs (sid , ssid , SK).

class OpaqueServer:
    def srvSession(pub, rec):
        #   decaf_255_point_t Alpha

        resp = Opaque_ServerSession()

        # (a) Checks that α ∈ G^∗ . If not, outputs (abort, sid , ssid ) and halts
        #   if(DECAF_SUCCESS!=decaf_255_point_decode(Alpha, pub->alpha, DECAF_FALSE)) return 1

        # (b) Retrieves file[sid] = {k_s, S_pri, S_pub, U_pub, c}
        # provided as parameter rec

        # (c) Picks x_s ←_R Z_q
        #   Todo: the line from libsphinx may be wrong. It should generate a random number.
        #   uint8_t x_s[DECAF_X25519_PRIVATE_BYTES];
        x_s = utils.random(DECAF_X25519_PRIVATE_BYTES)

        # computes β := α^k_s
        #   decaf_255_point_t Beta
        #   decaf_255_scalar_t K_s
        #   decaf_255_scalar_decode_long(K_s, rec->k_s, 32)
        resp.beta = bindings.crypto_scalarmult(pub.alpha, rec.k_s)
        #   decaf_255_point_scalarmul(Beta, Alpha, K_s);
        #   decaf_255_point_encode(resp->beta, Beta);

        # X_s := g^x_s
        resp.X_s = public.PrivateKey(x_s).public_key.encode()

        # (d) Computes K := KE(S_pri, x_s, U_pub, X_u) and SK := f_K(0)
        # paper instantiates HMQV, we do only triple-dh
        sk = server_kex(sk, rec.S_pri, x_s, rec.U_pub, pub.X_u)
        if sk != 0:
            raise Exception('server_kex execution error')

        # (e) Sends β, X_s and c to U
        resp.c = rec.c
        # also send salt
        resp.salt = rec.salt

        # (f) Outputs (sid , ssid , SK).
        # e&f handled as parameters

        return resp, sk


    # S records file[sid ] := {k_s, p_s, P_s, P_u, c}.
    def saveUser(sec, pub, rec):
        #if sec.__class__.__name__ != "":
        rec.k_s = sec.k_s
        rec.S_pri = sec.S_pri
        rec.S_pub = pub.S_pub
        return rec


    # initUser: S
    # (1) checks α ∈ G^∗ If not, outputs (abort, sid , ssid ) and halts
    # (2) generates k_s ←_R Z_q,
    # (3) computes: β := α^k_s,
    # (4) finally generates: S_pri ←_R Z_q, S_pub := g^S_pri
    def opaque_initUser(alpha, sec, pub):
        # (a) Checks that α ∈ G^∗ . If not, outputs (abort, sid , ssid ) and halts;
        # if(DECAF_SUCCESS!=decaf_255_point_decode(Alpha, alpha, DECAF_FALSE)) return 1;

        # k_s ←_R Z_q
        sec.k_s = utils.random(DECAF_X25519_PRIVATE_BYTES)  # random server user key
        # p_s ←_R Z_q
        sec.S_pri = utils.random(DECAF_X25519_PRIVATE_BYTES)  # random server long-term key

        # β := α^k_s
        #   decaf_255_point_t Beta;
        #   decaf_255_scalar_t K_s;
        #   decaf_255_scalar_decode_long(K_s, sec->k_s, 32);
        #   decaf_255_point_scalarmul(Beta, Alpha, K_s);
        #   decaf_255_point_destroy(Alpha);
        #   decaf_255_scalar_destroy(K_s);
        #   decaf_255_point_encode(pub->beta, Beta);
        #   decaf_255_point_destroy(Beta);
        pub.beta = bindings.crypto_scalarmult(alpha, sec.k_s)

        # S_pub := g^S_pri
        #   decaf_x25519_derive_public_key(pub->P_s, sec->p_s);
        S_pri = public.PrivateKey(sec.S_pri)
        pub.S_pub = S_pri.public_key.encode()

        return sec, pub


if __name__ == "__main__":

    # Testing initUser
    import os

    sec = Opaque_RegisterSec()
    pub = Opaque_RegisterPub()

    alpha, r = newUser(b'sjfpojwepog')

    resec, repub = opaque_initUser(alpha, sec, pub)
    #print(repub)
    #print(repub.beta, repub.S_pub) if repub else print('error')


    # Testing saveUser
    import os

    sec = Opaque_RegisterSec()
    pub = Opaque_RegisterPub()
    rec = Opaque_UserRecord()

    alpha = opaque_newUser(b'sjfpojwepog')
    resec, repub = opaque_initUser(alpha, sec, pub)

    # register

    rerec = opaque_saveUser(resec, repub, rec)

    print(rerec.__dict__)
    print(rerec) if rerec else print('error')
