from headers import *


# for the super-paranoid: use opaque_oprf() instead of this function for more heating
def opaque_f(k, val):
    """
    Returns the keyed hash of 32 value bytes.

    :param k: the key
    :type k: bytes
    :param val: the digest input byte sequence
    :type val: int
    :return: The hashed message.
    :rtype: bytes
    """

    # hash for the result res = f_k(val)
    v = bytearray()
    for iter in range(32):
        v.insert(iter, val)

    # hash size is hardcoded at DECAF_X25519_PUBLIC_BYTES
    res = hash.blake2b(data=bytes(v), digest_size=DECAF_X25519_PUBLIC_BYTES, key=k)

    if DEBUG:
        print("opaque_f = {}".format(res))

    return res
