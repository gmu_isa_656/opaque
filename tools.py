import math


class type2type:
    # B: bytes; b: bits.

    @staticmethod
    def i2B(i):
        x = int(i)
        return i.to_bytes((i.bit_length() + 7) // 8, "big")

    @staticmethod
    def B2i(B):
        return int.from_bytes(B, "big")

    @staticmethod
    def s2i(s):
        i = 0
        for char in s:
            if i > 0:
                i = i*256
            i = i + ord(char)
        return i

    @staticmethod
    def i2s(i):
        import math
        s = ""
        power = math.log(i, 256)
        while power > 0:
            digit = i % 256
            s = str(chr(digit)) + s
            i = (i - digit) / 256
            power -= 1
        return s

    @staticmethod
    def s2b(i):
        pass

    @staticmethod
    def b2s(i):
        pass

    @staticmethod
    def s2B(i):
        pass

    @staticmethod
    def B2s(i):
        pass


if __name__ == "__main__":
    i = 1000000000000000000000000
    B = type2type.i2B(i)
    print(B)
    i2 = type2type.B2i(B)
    print(i2)
