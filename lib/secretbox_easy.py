from nacl.bindings.crypto_secretbox import *
# added by my self


def crypto_secretbox_easy(message, nonce, key):
    if len(nonce) != crypto_secretbox_NONCEBYTES:
        raise Exception('Nonce is {} bytes, expected {} bytes'.format(len(nonce), crypto_secretbox_NONCEBYTES))

    if len(key) != crypto_secretbox_KEYBYTES:
        raise Exception('Key is {} bytes, expected {} bytes'.format(len(key), crypto_secretbox_KEYBYTES))

    ciphertext = ffi.new('unsigned char[]', crypto_secretbox_MACBYTES + len(message))
    res = lib.crypto_secretbox_easy(ciphertext, message, len(message), nonce, key)
    ensure(res == 0, "Encryption failed", raising=exc.CryptoError)

    ciphertext = ffi.buffer(ciphertext, len(message))
    return ciphertext

def crypto_secretbox_open_easy(ciphertext, nonce, key):

    if len(key) != crypto_secretbox_KEYBYTES:
        raise exc.ValueError("Invalid key")

    if len(nonce) != crypto_secretbox_NONCEBYTES:
        raise exc.ValueError("Invalid nonce")

    plaintext = ffi.new('unsigned char[]', len(ciphertext) - crypto_secretbox_MACBYTES)
    res = lib.crypto_secretbox_open_easy(plaintext, ciphertext, len(ciphertext), nonce, key)
    ensure(res == 0, "Decryption failed. Ciphertext failed verification",
           raising=exc.CryptoError)

    plaintext = ffi.buffer(plaintext, len(ciphertext))
    return plaintext

if __name__ == '__main__':
    import os
    nonce = os.urandom(24)
    key = os.urandom(32)
    ct = crypto_secretbox_easy(b'secret box test', nonce, key)
    print(ct)
    pt = crypto_secretbox_open_easy(ct, nonce, key)
    print(pt)
